$(document).ready(function() {
	$("#menu-btn").click(function() {
		$("#header-menu").show()
	})

	$("#close").click(function() {
		$("#header-menu").hide()
	})
})
//scroll menu
$(document).ready(function(){

        var $menu = $("#scroll-menu");

        $(window).scroll(function(){
            if ( $(this).scrollTop() > 700 && $menu.hasClass("default") ){
                $menu.removeClass("default").addClass("fixed");
            } else if($(this).scrollTop() <= 700 && $menu.hasClass("fixed")) {
                $menu.removeClass("fixed").addClass("default");
            }
        });//scroll
    });

$(document).ready(function(){
	$("#scroll-menu, #header-menu").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top-80}, 800);
	});
});
